$(document).ready(function () {

  const formatNum = n => {
    if (n < 1e3) return n;
    if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + "K";
    if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + "M";
  };

  if (!(localStorage['igprofile'] && localStorage['igfollower'])) {
    $.getJSON("https://www.instagram.com/nanon_korapat/?__a=1", function (igdata) {
      localStorage.setItem('igprofile', igdata.graphql.user.profile_pic_url_hd);
      localStorage.setItem('igfollower', igdata.graphql.user.edge_followed_by.count);
      $("#igprofile").attr('src', igdata.graphql.user.profile_pic_url_hd);
      $("#igfollower").text(formatNum(igdata.graphql.user.edge_followed_by.count));
    });
  } else {
    if (!(formatNum(localStorage.getItem('igfollower')).includes('2'))) {
      $.getJSON("https://www.instagram.com/nanon_korapat/?__a=1", function (igdata) {
        localStorage.setItem('igfollower', igdata.graphql.user.edge_followed_by.count);
        $("#igfollower").text(formatNum(igdata.graphql.user.edge_followed_by.count));
      });
    }
    //if (!(/105550761_327170421613701_2438633456899782628_n.jpg/i.test(localStorage.getItem('igprofile')))) {
      $.getJSON("https://www.instagram.com/nanon_korapat/?__a=1", function (igdata) {
        localStorage.setItem('igprofile', igdata.graphql.user.profile_pic_url_hd);
        $("#igprofile").attr('src', igdata.graphql.user.profile_pic_url_hd);
      });
    //}
  }

  if (!(localStorage['ytprofile'] && localStorage['ytfollower'])) {
    $.getJSON("https://www.googleapis.com/youtube/v3/channels?part=snippet,statistics&id=UCbDQXwjWSnnfOAeAZZ5O_rA&key=AIzaSyDNdVGhM-Fye8nSfAJo0Sye-P1914uDUy4", function (ytdata) {
      localStorage.setItem('ytprofile', ytdata.items[0].snippet.thumbnails.medium.url);
      localStorage.setItem('ytfollower', ytdata.items[0].statistics.subscriberCount);
      $("#ytprofile").attr('src', ytdata.items[0].snippet.thumbnails.medium.url);
      $("#ytfollower").text(formatNum(ytdata.items[0].statistics.subscriberCount));
    });
  } else {
    $.getJSON("https://www.googleapis.com/youtube/v3/channels?part=statistics&id=UCbDQXwjWSnnfOAeAZZ5O_rA&key=AIzaSyDNdVGhM-Fye8nSfAJo0Sye-P1914uDUy4", function (ytdata) {
      if (localStorage.getItem('ytfollower') !== ytdata.items[0].statistics.subscriberCount) {
        localStorage.setItem('ytfollower', ytdata.items[0].statistics.subscriberCount);
        $("#ytfollower").text(formatNum(ytdata.items[0].statistics.subscriberCount));
      }
    });
  }

  let igprofile = localStorage.getItem('igprofile'),
    igfollower = localStorage.getItem('igfollower'),
    ytprofile = localStorage.getItem('ytprofile'),
    ytfollower = localStorage.getItem('ytfollower'),
    twitterprofile = 'https://pbs.twimg.com/profile_images/1271750195450007552/Usb4toUV.jpg',
    twitterfollower = '394000',
    fbprofile = 'https://graph.facebook.com/NanonKorapat/picture?type=large'
  fbfollower = '116000'

  $("#igprofile").attr('src', igprofile);
  $("#igfollower").text(formatNum(igfollower));

  $("#twitterprofile").attr('src', twitterprofile);
  $("#twitterfollower").text(formatNum(twitterfollower));

  $("#fbprofile").attr('src', fbprofile);
  $("#fbfollower").text(formatNum(fbfollower));

  $("#ytprofile").attr('src', ytprofile);
  $("#ytfollower").text(formatNum(ytfollower));

});
// https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=mynameisnanon

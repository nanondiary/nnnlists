const formatView = n => {
  if (n < 1e3) return n;
  if (n >= 1e3 && n < 1e6) return +Math.trunc(n / 1e3) + "K";
  if (n >= 1e6 && n < 1e9) return +Math.trunc((n * 10) / 1e6) / 10 + "M";
};

if (!(sessionStorage['ep1'] && sessionStorage['ep2'] && sessionStorage['ep3'] && sessionStorage['ep4'] && sessionStorage['ep5'] && sessionStorage['fact'] && sessionStorage['naa1'] && sessionStorage['naa2'] && sessionStorage['ch1'] && sessionStorage['ch2'] && sessionStorage['ch3'] && sessionStorage['ch4'] && sessionStorage['ch5'] && sessionStorage['naa3'] && sessionStorage['naa4'] && sessionStorage['naa5'])) {
  $.getJSON("https://www.googleapis.com/youtube/v3/videos?part=statistics&id=sCU_F8VOjgI,YVO3dyFCbII,1c1iOkzZvFM,sY7QaMfmv90,bgZb6UAVUZc,fiKbv8qZQ4I,K0l7B5y_raI,0ucEe1sNhls,9z6AowD-qkU,y_bTKJpA25o,pORk2dR5wFs,-C6Tcm6mhQQ,ACipftkAB-g,g7YYRF1LJto,ix88pcPfJB4,OniAmY86EmE&key=AIzaSyDNdVGhM-Fye8nSfAJo0Sye-P1914uDUy4", function (data) {
    sessionStorage.setItem('ep1', data.items[0].statistics.viewCount);
    sessionStorage.setItem('ep2', data.items[1].statistics.viewCount);
    sessionStorage.setItem('ep3', data.items[2].statistics.viewCount);
    sessionStorage.setItem('ep4', data.items[3].statistics.viewCount);
    sessionStorage.setItem('ep5', data.items[4].statistics.viewCount);
    sessionStorage.setItem('fact', data.items[5].statistics.viewCount);
    sessionStorage.setItem('naa1', data.items[6].statistics.viewCount);
    sessionStorage.setItem('ch1', data.items[7].statistics.viewCount);
    sessionStorage.setItem('ch2', data.items[8].statistics.viewCount);
    sessionStorage.setItem('naa2', data.items[9].statistics.viewCount);
    sessionStorage.setItem('ch3', data.items[10].statistics.viewCount);
    sessionStorage.setItem('ch4', data.items[11].statistics.viewCount);
    sessionStorage.setItem('ch5', data.items[12].statistics.viewCount);
    sessionStorage.setItem('naa3', data.items[13].statistics.viewCount);
    sessionStorage.setItem('naa4', data.items[14].statistics.viewCount);
    sessionStorage.setItem('naa5', data.items[15].statistics.viewCount);
    $("#ep1").text(formatView(sessionStorage.getItem('ep1')));
    $("#ep2").text(formatView(sessionStorage.getItem('ep2')));
    $("#ep3").text(formatView(sessionStorage.getItem('ep3')));
    $("#ep4").text(formatView(sessionStorage.getItem('ep4')));
    $("#ep5").text(formatView(sessionStorage.getItem('ep5')));
    $("#fact").text(formatView(sessionStorage.getItem('fact')));
    $("#naa1").text(formatView(sessionStorage.getItem('naa1')));
    $("#ch1").text(formatView(sessionStorage.getItem('ch1')));
    $("#ch2").text(formatView(sessionStorage.getItem('ch2')));
    $("#naa2").text(formatView(sessionStorage.getItem('naa2')));
    $("#ch3").text(formatView(sessionStorage.getItem('ch3')));
    $("#ch4").text(formatView(sessionStorage.getItem('ch4')));
    $("#ch5").text(formatView(sessionStorage.getItem('ch5')));
    $("#naa3").text(formatView(sessionStorage.getItem('naa3')));
    $("#naa4").text(formatView(sessionStorage.getItem('naa4')));
    $("#naa5").text(formatView(sessionStorage.getItem('naa5')));
  })
} else {
  $("#ep1").text(formatView(sessionStorage.getItem('ep1')));
  $("#ep2").text(formatView(sessionStorage.getItem('ep2')));
  $("#ep3").text(formatView(sessionStorage.getItem('ep3')));
  $("#ep4").text(formatView(sessionStorage.getItem('ep4')));
  $("#ep5").text(formatView(sessionStorage.getItem('ep5')));
  $("#fact").text(formatView(sessionStorage.getItem('fact')));
  $("#naa1").text(formatView(sessionStorage.getItem('naa1')));
  $("#ch1").text(formatView(sessionStorage.getItem('ch1')));
  $("#ch2").text(formatView(sessionStorage.getItem('ch2')));
  $("#naa2").text(formatView(sessionStorage.getItem('naa2')));
  $("#ch3").text(formatView(sessionStorage.getItem('ch3')));
  $("#ch4").text(formatView(sessionStorage.getItem('ch4')));
  $("#ch5").text(formatView(sessionStorage.getItem('ch5')));
  $("#naa3").text(formatView(sessionStorage.getItem('naa3')));
  $("#naa4").text(formatView(sessionStorage.getItem('naa4')));
  $("#naa5").text(formatView(sessionStorage.getItem('naa5')));
}
